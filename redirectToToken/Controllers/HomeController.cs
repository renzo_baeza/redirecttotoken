﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace redirectToToken.Controllers
{
    public class HomeController : Controller
    {
        public string token;
        public ActionResult Index()
        {
            this.token = Request.Form["token"];
            ViewBag.name = this.token;
            //return Redirect("http://www.google.com");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}